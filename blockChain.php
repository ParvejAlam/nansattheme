<?php require_once("header.php"); ?>
<!-- course Header start tag-->
<div class="courseHeader">
    <div class="container">
            <h1>Block Chain</h1>
            <div class="bannerTagLine">Machine Learning with Python</div>
            <div class="taglineBorder">
                <div>
                    <div></div>
                </div>
            </div>
    </div>
</div>
<!-- course Header end tag -->
<!-- breadcrumb start tag-->
<nav class="customBreadcrumb">      
    <div class="container">
        <a href="javascript:void(0);">Home</a><i class="fa fa-chevron-right"></i><a href="javascript:void(0);">All courses</a><i class="fa fa-chevron-right"></i> Block Chain
    </div>

</nav>
<!-- breadcrumb end tag -->

<!-- Main Content Start from here -->
<div class="container">
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="boxContainer">
                <h2 class="courseTitle">Block Chain</h2>
                <div class="singleCourse">
                    <div class="clearfix">
                        <div class="pull-left meta_pull">

                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa-icon-stm_icon_teacher"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Faculty</div>
                                            <div class="value">Mohit Bansal</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_category"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Category</div>
                                            <div class="value">Machine Learning</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_users"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Students</div>
                                            <div class="value">30 per batch</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_clock"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Duration</div>
                                            <div class="value">3 Months</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_bullhorn"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Classes</div>
                                            <div class="value">30</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_earth"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Live Project</div>
                                            <div class="value">1</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-file-text-o"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Cetification</div>
                                            <div class="value"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                                 
                                            
                        </div> <!-- meta pull -->
                        

                    </div>
                    <div class="clear"></div>
                </div>
                <div class="courseImg">
                    <img src="assets/img/blockchainBanner.png" alt="Course Name">
                </div>
            </div>

        </div>  <!-- col-xs-9 end -->
        <div class="col-lg-3 col-md-3">     
            <div class="topRight">
                <div class="row">
                    <div class="col-xs-12">
                        <label>PRICE</label>
                        <p><i class="fa fa-inr"></i> 25,000/-</p>
                    </div>
                </div>
                <a href="javascript:;" class="affiliate">Enroll Now</a>
                <div class="enrollBox">
                    <form method="post" action="enrollExec.php">
                        <input type="text" name="name" placeholder="Whats your name..."> 
                        <input type="text" name="contact" placeholder="Whats your Phone..."> 
                        <input type="text" name="emailId" placeholder="Whats your Email..."> 
                        <input type="hidden" name="course" value="block Chain"> 
                        <input type="text" name="college" placeholder="College/Company Name..."> 
                        <input type="submit" name="btn" value="Submit">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <!-- <section class="content m-t-40">
                    <h3 class="sectionTitle">COURSE DESCRIPTION</h3>
                    <div class="m-b-30">
                        <p>
                            This course has been developed to meet the growing need for blockchain-focused QA Engineers. The course provides hands-on examples of this type of rigorous exploratory testing and explains how to set up different test environments in order to do it.
                        </p>
                        <p>
                            Assuming the QA engineer – like any modern testing professional on an Agile project – is involved at the earliest stage of the project, he or she needs to ask questions about transaction fees, latency, privacy and data compliance. You will learn to specify acceptance criteria that can later be used as the basis for automating your acceptance tests, with quizzes, screencasts and exercises to test your understanding of the requirements. Test automation is a critical part of the QA process. You will learn about the most common test libraries and how to run your suite in a continuous integration environment.
                        </p>
                    </div>
                </section>

                <div class="multiseparator m-t-60"></div> -->
                <section class="m-t-20">
                    <h3 class="sectionTitle">CURRICULUM</h3>
                    <!-- <h5 class="boxHeading">Section 1: Introduction to Handstands</h5> -->
                    <div class="collapseGroup">
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>1</td>
                                    <td><strong>Introduction to BlockChain <i class="fa fa-sort-down"></i></strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <!-- <p>
                                    The introduction of cryptocurrencies, specifically Bitcoin, has brought the concept of blockchain technology into the mainstream. A blockchain is a continuously growing distributed database that protects against tampering and revision of data.
                                </p> -->
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>2</td>
                                    <td><strong>Concepts & Architecture <i class="fa fa-sort-down"></i></strong></td>
                                    <td>5 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                              
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>3</td>
                                    <td><strong>Solidity or Go <i class="fa fa-sort-down"></i></strong></td>
                                    <td>5 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>4</td>
                                    <td><strong>Ethereum or Hyperledger Platform Overview <i class="fa fa-sort-down"></i></strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>5</td>
                                    <td><strong>Development & Testing <i class="fa fa-sort-down"></i></strong></td>
                                    <td>8 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>Smart Contract</li>
                                    <li>Ico</li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>6</td>
                                    <td><strong>Final Project <i class="fa fa-sort-down"></i></strong></td>
                                    <td>8 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <p>
                                    Any Transaction use case
                                </p>
                            </div>
                        </div>
                    </div>


                </section>
                <div class="multiseparator m-t-60"></div>
                <section class="m-t-30">
                    <h3 class="sectionTitle">ABOUT INSTRUCTOR</h3>
                    <a href="javascript:void(0);">
                        <div class="instructorBox">
                            <img src="assets/img/instructor.jpg" alt="instructor">
                            <div>
                                <div class="name">
Mohit Bansal</div>
                                <p class="desc">Chief Technology Officer</p>
                            </div>
                            <div class="socialLink">
                                <a href="https://www.facebook.com/mohit.bansal.520">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/in/mohit-bansal-bb463670/">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            <!-- 
                                <a href="javascript:void(0);">
                                    <i class="fa fa-twitter"></i>
                                </a>
                                <a href="javascript:void(0);">
                                    <i class="fa fa-google-plus"></i>
                                </a> 
                            -->
                            </div>
                        </div>
                    </a>
                <!-- 
                    <div class="clear"></div>
                    <p class="instructorDescription">
                        Before engineering, I developed and implemented CI (Continuous Improvement) management systems in companies across America in various industries to include a military finance organization.
                    </p>
                 -->
             </section>
                <div class="multiseparator m-t-50"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">


    </div>
</div>
</div>
<?php require_once("footer.php"); ?>