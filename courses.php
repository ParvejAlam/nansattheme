<?php require_once("header.php"); ?>
<!-- course Header start tag-->
<div class="courseHeader">
    <div class="container">
            <h1>How to Become a Startup Founder</h1>
            <div class="bannerTagLine">Learn everything you need to know to launch your startup!</div>
            <div class="taglineBorder">
                <div>
                    <div></div>
                </div>
            </div>
    </div>
</div>
<!-- course Header end tag -->
<!-- breadcrumb start tag-->
<nav class="customBreadcrumb">      
    <div class="container">
        <a href="javascript:void(0);">Home</a><i class="fa fa-chevron-right"></i><a href="javascript:void(0);">All courses</a><i class="fa fa-chevron-right"></i> Web Development
    </div>

</nav>
<!-- breadcrumb end tag -->

<!-- Main Content Start from here -->
<div class="container">
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="boxContainer">
                <h2 class="courseTitle">How to Become a Startup Founder</h2>
                <div class="singleCourse">
                    <div class="clearfix">
                        <div class="pull-left meta_pull">
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa-icon-stm_icon_teacher"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Teacher</div>
                                            <div class="value">Sarah Johnson</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                                        
                                <div class="pull-left xs-product-cats-left">
                                    <div class="meta-unit categories clearfix">
                                        <div class="pull-left">
                                            <i class="fa-icon-stm_icon_category"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label h6">Category:</div>
                                            <div class="value h6">
                                                <a href="javascript:;">Business &amp; Management<span></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                            
                        </div> <!-- meta pull -->
                        

                    </div>
                    <div class="clear"></div>
                </div>
                <div class="courseImg">
                    <img src="assets/img/courseImg.jpg" alt="Course Name">
                </div>
            </div>

        </div>  <!-- col-xs-9 end -->
        <div class="col-lg-3 col-md-3">     
            <div class="topRight">
                <div class="row">
                    <div class="col-xs-12">
                        <label>PRICE</label>
                        <p>Free!</p>
                    </div>
                </div>
                <a href="javascript:;" class="affiliate">Affiliated Course</a>
                <table>
                    <tbody>
                        <tr>
                            <td class="icon"><i class="fa-icon-stm_icon_users"></i></td>
                            <td class="data">300 Students</td>
                        </tr>
                        <tr>
                            <td class="icon"><i class="fa-icon-stm_icon_clock"></i></td>
                            <td class="data">Duration: 4 hours</td>
                        </tr>
                        <tr>
                            <td class="icon"><i class="fa-icon-stm_icon_bullhorn"></i></td>
                            <td class="data">Lectures: 1</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <section class="content m-t-40">
                    <h3 class="sectionTitle">COURSE DESCRIPTION</h3>
                    <div class="m-b-30">
                        <p>
                            Maecenas cursus mauris libero, a imperdiet enim pellentesque id. Aliquam erat volutpat. Suspendisse sit amet sapien at risus efficitur sagittis. Pellentesque non ullamcorper justo. Vivamus commodo, sem et vestibulum eleifend, erat odio tristique enim, nec tempus tortor ligula in neque. Vestibulum eu commodo ante. Nunc volutpat nec diam a congue.
                        </p>
                        <p>
                            Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Sed quis rutrum tellus, sit amet viverra felis. Cras sagittis sem sit amet urna feugiat rutrum. Nam nulla ipsum, venenatis malesuada felis quis, ultricies convallis neque. Pellentesque tristique fringilla tempus. Vivamus bibendum nibh in dolor pharetra, a euismod nulla dignissim. Aenean viverra tincidunt nibh, in imperdiet nunc. Suspendisse eu ante pretium, consectetur leo at, congue quam. Nullam hendrerit porta ante vitae tristique. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum ligula libero, feugiat faucibus mattis eget, pulvinar et ligula.
                        </p>
                    </div>
                    <h6>Requirements</h6>
                    <div class="row">
                        <div class="col-xs-12 col-ms-6">
                            <ul>
                                <li>Donec porta ultricies urna, faucibus magna dapibus.</li>
                                <li>Etiam varius tortor ut ligula facilisis varius in a leo.</li>
                                <li>Folutpat tempor tur duis mattis dapibus, felis amet.</li>
                            </ul>
                        </div>  
                        <div class="col-xs-12 col-ms-6">
                            <ul>
                                <li>Donec porta ultricies urna, faucibus magna dapibus.</li>
                                <li>Etiam varius tortor ut ligula facilisis varius in a leo.</li>
                                <li>Folutpat tempor tur duis mattis dapibus, felis amet.</li>
                            </ul>
                        </div>      
                    </div>
                    <h6 class="m-t-30">What is the target audience?</h6>
                    <div class="row">
                        <div class="col-xs-12">
                            <ul>
                                <li>This course is intended for anyone interested in learning to master his or her own body.</li>
                                <li>This course is aimed at beginners, so no previous experience with hand balancing skillts is necessary</li>
                            </ul>
                        </div>
                    </div>
                    <p class="m-t-30">
                        Aenean viverra tincidunt nibh, in imperdiet nunc. Suspendisse eu ante pretium, consectetur leo at, congue quam. Nullam hendrerit porta ante vitae tristique. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum ligula libero, feugiat faucibus mattis eget, pulvinar et ligula.
                    </p>
                </section>

                <div class="multiseparator m-t-60"></div>
                <section class="m-t-60">
                    <h3 class="sectionTitle">CURRICULUM</h3>
                    <h5 class="boxHeading">Section 1: Introduction to Handstands</h5>
                    <div class="collapseGroup">
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>1</td>
                                    <td><strong>Welcome to the Course! <i class="fa fa-sort-down"></i></strong></td>
                                    <td>Private</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <p>
                                    This final lecture will evaluate expectations and real gained knowledge and experience. Lecturer will share some self-development sources for further independent training of course participants.
                                </p>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>2</td>
                                    <td><strong>Course Introduction and Manual  <i class="fa fa-sort-down"></i></strong></td>
                                    <td>PDF file</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <p>
                                    Welcome to your second lesson review.
                                </p>
                                <p>
                                    Here we will talk about your purposes on course and goal you want to reach
                                </p>
                            </div>
                        </div>
                    </div>

                    <h5 class="boxHeading m-t-30">Section 2: Reference Material, Moodboards and Mind Mapping</h5>
                    <div class="collapseGroup">
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>3</td>
                                    <td><strong>Advanced Wrist Preparation  <i class="fa fa-sort-down"></i></strong></td>
                                    <td>1:20:00</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <p>
                                    Wrist Strengthening
                                </p>
                                <p>
                                    While your wrists will certain get stronger from practice and grow accustomed to the stress of the skill, a basic amount of wrist strengthening exercises for several weeks can only help things. I’d recommend working wrist curls and reverse wrist curls for around 6-10 reps for 3 sets. I also strongly recommend trying some sledgehammer levering. Work in 2-3 sets of 3-5 reps. In particular, exercises 1 and 3 are fantastic for building wrist strength and they are much harder than they look! Start with them to get the hang of sledgehammer work before you progress to the other two exercises. I don’t want somebody putting a hole through their floor or their face because they rushed things!
                                </p>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>4</td>
                                    <td><strong>The logo design briefing with the client  <i class="fa fa-sort-down"></i></strong></td>
                                    <td>Private</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <p>
                                    The content of this lesson is locked. To unlock it, you need to Buy this Course.
                                </p>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>5</td>
                                    <td><strong>Midterm test  <i class="fa fa-sort-down"></i></strong></td>
                                    <td>20 questions</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <p>
                                    This test will reveal how well you are acquiring the new material based on a short test of twenty questions and a brief written assignment.
                                </p>
                            </div>
                        </div>
                    </div>



                    <h5 class="boxHeading m-t-30">Section 3: Sketching out Ideas</h5>
                    <div class="collapseGroup">
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>6</td>
                                    <td><strong>Practical sketching activity <i class="fa fa-sort-down"></i></strong></td>
                                    <td>1:20:00</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>
                                        The main objective to the sketching process is to generate super rough thumbnail sketches&nbsp;of what we feel best visually communicates the highlighted words from our mind maps.
                                    </li>
                                    <li>
                                        Take as much time as you need for this step —&nbsp;this might be 10 minutes or it might be 10 days.
                                    </li>
                                    <li>
                                        Personally, I like to work quickly and try not to analyze or elaborate too much.
                                    </li>
                                    <li>
                                        Now, that doesn’t mean you should only create a handful of sketches.
                                    </li>
                                    <li>
                                        Even though this step only took a couple of hours, I was still able to put over 100 thumbnails on paper.
                                    </li>
                                    <li>
                                        The whole point of this process is to flush out the bad ideas and narrow down the good ones&nbsp;until we find that one layout that really speaks to us.
                                    </li>
                                    <li>
                                        Also, keep in mind the project brief and have your list in front of you as a reference to avoid getting sidetracked.
                                    </li>
                                    <li>
                                        Remember—detail is not needed. Simply flush out the bad ideas and find a great direction.
                                    </li>
                                    <li>
                                        Once I feel I have a good direction with the sketches,&nbsp;I’m now ready to take a quick photo with my phone and import it into Illustrator.
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>7</td>
                                    <td><strong>Midterm test <i class="fa fa-sort-down"></i></strong></td>
                                    <td>10 questions</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <p>
                                    This test will reveal how well you are acquiring the new material based on a short quiz of ten questions and a short writing assignment.
                                </p>
                            </div>
                        </div>
                    </div>

                    <h5 class="boxHeading m-t-30">Section 4: Conclusions and Evaluation</h5>
                    <div class="collapseGroup">
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>8</td>
                                    <td><strong>Collecting Together Reference Material  <i class="fa fa-sort-down"></i></strong></td>
                                    <td>References</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <p>
                                    This final lecture will evaluate expectations and real gained knowledge and experience. Lecturer will share some self-development sources for further independent training of course participants.              
                                </p>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>9</td>
                                    <td><strong>Final Assessment Task <i class="fa fa-sort-down"></i></strong></td>
                                    <td>Final</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <p>
                                    Every course participant will be given a personalised practical task for creating branding logo design for real companies.
                                </p>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>5</td>
                                    <td><strong>Presentation <i class="fa fa-sort-down"></i></strong></td>
                                    <td>End of course</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <p>
                                    Every course participant will have to present his final assessment task result in his chosen individual manner to the audience.
                                </p>
                            </div>
                        </div>
                    </div>

                </section>
                <div class="multiseparator m-t-60"></div>
                <section class="m-t-30">
                    <h3 class="sectionTitle">ABOUT INSTRUCTOR</h3>
                    <a href="javascript:void(0);">
                        <div class="instructorBox">
                            <img src="assets/img/instructor.jpg" alt="instructor">
                            <div>
                                <div class="name">
Mohit Bansal</div>
                                <p class="desc">Chief Technology Officer</p>
                            </div>
                            <div class="socialLink">
                                <a href="https://www.facebook.com/mohit.bansal.520">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/in/mohit-bansal-bb463670/">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            <!-- 
                                <a href="javascript:void(0);">
                                    <i class="fa fa-twitter"></i>
                                </a>
                                <a href="javascript:void(0);">
                                    <i class="fa fa-google-plus"></i>
                                </a> 
                            -->
                         </div>
                        </div>
                    </a>
                    <div class="clear"></div>
                    <!-- <p class="instructorDescription">
                        Before engineering, I developed and implemented CI (Continuous Improvement) management systems in companies across America in various industries to include a military finance organization.
                    </p> -->
                </section>
                <div class="multiseparator m-t-60"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">


    </div>
</div>
<?php require_once("footer.php"); ?>