<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Nansat Education</title>
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">

 
  <link rel="stylesheet" href="assets/css/font.css">
 <link rel="stylesheet" href="assets/css/style.css">
 <link rel="stylesheet" href="assets/css/customStyle.css">
 <script src="assets/js/jquery.js"></script>
 <?php
 $localServer="/nansatTheme/";
 // $localServer="http://nansat.com/";
 // echo $_SERVER["PHP_SELF"];
if($_SERVER["PHP_SELF"]==$localServer || $_SERVER["PHP_SELF"]==$localServer."index.php" || $_SERVER["PHP_SELF"]=="/index.php"){
  ?>
  <style type="text/css">
    header .navigation-bar {
        background-color: transparent;
    }
  </style>
  <?php
}
 ?>
</head>
<body>
<div class="wrapper">
<!-- header -->
 <div class="container-fluid top-strip">
  <div class="row">
  <div class="container">
  <div class="row">
  <div class="pull-right hidden-xs">
						<ul>
						<li>
															<a href="">
									<i class="fa fa-user"></i> Login								</a>
								<span class="vertical_divider">|</span>
								<a href="">Register</a>
								</li>
							<ul>						
					</div>
    <div class="pull-right info  xs-pull-left">
	   <ul>
	   <li><i class="fa fa-clock-o"></i> Sun - Sat 8.00 - 18.00</li>
       
       <li><i class="fa fa-phone"></i> 09313580700</li>
	   </ul>
	</div>
 </div>
 </div>
 </div>
 </div>
<header>

<div class="col-xs-12 col-col-xs-12 navigation-bar">
				   <div class="row">
				       <div class="container">
					      <div class="row">
						      <nav class="navbar navbar-inverse">
                                <div class="container-fluid">
								<div class="row">
                                <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>                        
                                </button>
                                <a class="navbar-brand" href="<?php echo $localServer; ?>"><img src="assets/img/logo.png" alt="logo"></a>
                               </div>
                               <div class="collapse navbar-collapse" id="myNavbar">
                               <ul class="nav navbar-right navbar-nav">
                                <li><a href="<?php echo $localServer."index.php" ?>" class="active">Home</a></li>
                                <li><a href="<?php echo $localServer."index.php" ?>#courses">COURSES</a></li>
                                <li><a href="<?php echo $localServer."index.php" ?>#achievement">OUR ACHIEVEMENTS</a></li>
                                <li><a href="<?php echo $localServer."index.php" ?>#success-stories">SUCCESS STORIES</a></li>
                                <li><a href="<?php echo $localServer."index.php" ?>#enroll">Enroll Now</a></li>
                                
                               </ul>
                               </div>
                               </div>
                              </div>
                             </nav>
						  </div>
					   </div>
				   </div>
				</div>
</header>
<!-- header -->