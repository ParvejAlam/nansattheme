<!-- footer -->
<div id="footer" class="page-footer container-fluid">
   <div class="row">
   <div class="col-xs-12 footer-wrapper">
     <div class="footer-top">
   
	     <div class="container">
		     <div class="row">
			    <div class="col-xs-12 footer-details">
				     <div class="row">
					    <div class="col-md-3 col-sm-3 col-xs-12  about-us box">
						    <h3 class="title">ABOUT US</h3>
							<p>Industry is facing a skills gap in the key technology areas like AI, Machine Learning, Block Chain, Deep Tech etc. Market appetite for these skills is far outstripping supply today; At NanSat, we are taking stock and investing in bringing these latest skills to students to ensure that we contribute our bit towaards this cause. Along with training students with the latest technologies and programming languages, we also connect them to software companies to get them placed
</p>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 quik-links box">
						    <h3 class="title">QUICK LINKS</h3>
							<ul class="">
				                <li><a href="<?php echo $localServer."index.php" ?>" class="active">Home</a></li>
                                <li><a href="<?php echo $localServer."index.php" ?>#courses">COURSES</a></li>
                                <li><a href="<?php echo $localServer."index.php" ?>#achievement">OUR ACHIEVEMENTS</a></li>
                                <li><a href="<?php echo $localServer."index.php" ?>#success-stories">SUCCESS STORIES</a></li>
                                <li><a href="<?php echo $localServer."index.php" ?>#enroll">Contact Us</a></li>
			                </ul>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12  contact-address box">
						    <h3 class="title">CONTACT US</h3>
							<ul>
							<li class="first"><div class="icon"><i class="fa-icon-stm_icon_pin"></i></div><div class="text">2nd floor, 178 Sector 10, 
On <br>Sector 9/10 Dividing Road 
Faridabad - Haryana</div></li>
							<li><div class="icon"><i class="fa-icon-stm_icon_phone"></i></div><div class="text">09313580700</div></li>
							
							<li><div class="icon"><i class="fa fa-envelope"></i></div><div class="text"><a href="mailto:info@stylemixthemes.com">Nansatindia@gmail.com</a></div></li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12  social-links box">
						    <h3 class="title">SOCIAL NETWORK</h3>
							<ul class="">
							<li class="">
							<div class="flipper">
								<div class="front">
									<a href="http://www.facebook.com/Nansat-Commerce-1025272230913785/?skip_nax_wizard=true" target="_blank"><i class="fa fa-facebook"></i></a>
								</div>
								<div class="back">
									<a href="http://www.facebook.com/Nansat-Commerce-1025272230913785/?skip_nax_wizard=true" target="_blank"><i class="fa fa-facebook"></i></a>
								</div>
							</div>
						</li>
						<li class="">
							<div class="flipper">
								<div class="front">
									<a href="http://twitter.com/NansatCommerce" target="_blank"><i class="fa fa-twitter"></i></a>
								</div>
								<div class="back">
									<a href="http://twitter.com/NansatCommerce" target="_blank"><i class="fa fa-twitter"></i></a>
								</div>
							</div>
						</li>
						<li class="">
							<div class="flipper">
								<div class="front">
									<a href="http://plus.google.com/u/6/107534664525363723676?hl=en" target="_blank"><i class="fa fa-google-plus"></i></a>
								</div>
								<div class="back">
									<a href="http://plus.google.com/u/6/107534664525363723676?hl=en" target="_blank"><i class="fa fa-google-plus"></i></a>
								</div>
							</div>
						</li>
						<li class="">
							<div class="flipper">
								<div class="front">
									<a href="http://www.linkedin.com/in/nansat-commerce-aa18a2124?trk=nav_responsive_tab_profile&#10;&#9;" target="_blank"><i class="fa fa-linkedin"></i></a>
								</div>
								<div class="back">
									<a href="http://www.linkedin.com/in/nansat-commerce-aa18a2124?trk=nav_responsive_tab_profile&#10;&#9;" target="_blank"><i class="fa fa-linkedin"></i></a>
								</div>
							</div>
						</li>				
	        </ul>
						</div>
					 </div>
				</div>
			 </div>
		 </div>
	</div>
	</div>
	</div>
	</div>  
  <script src="assets/js/bootstrap.min.js"></script>
  <script>
  $(document).ready(function(){
  	$("body").on("focus focusout","input,select",function(){
  		$(this).next(".errMsg").fadeOut(500,function(){$(this).remove();});
  	});
  	$("body").on("submit",'form[action="enrollExec.php"]',function(){
  		if($('input[name="name"]').val()==""){
  			if($('input[name="name"]').next(".errMsg").text()=="")
  			$('input[name="name"]').after("<span class='errMsg'>Please Enter Your Name</span>");
  			return false;
  		}
  		else if($('input[name="contact"]').val()==""){
  			if($('input[name="contact"]').next(".errMsg").text()=="")
  			$('input[name="contact"]').after("<span class='errMsg'>Please Enter Your Number</span>");
  			return false;
  		}
  		else if(!$.isNumeric($('input[name="contact"]').val())){
  			if($('input[name="contact"]').next(".errMsg").text()=="")
  			$('input[name="contact"]').after("<span class='errMsg'>Phone Number Is Not Valid<span>");
  			return false;
  		}
  		else if($('input[name="contact"]').val().match(/^[789]\d{9}$/)==null){
  			if($('input[name="contact"]').next(".errMsg").text()=="")
  			$('input[name="contact"]').after("<span class='errMsg'>Phone Number Is Not Valid<span>");
  			return false;
  		}
  		else if($('input[name="emailId"]').val()==""){
  			if($('input[name="emailId"]').next(".errMsg").text()=="")
  			$('input[name="emailId"]').after("<span class='errMsg'>Please Enter Your email Id</span>");
  			return false;
  		}
  		else if($('select[name="course"]').val()==""){
  			if($('select[name="course"]').next(".errMsg").text()=="")
  			$('select[name="course"]').after("<span class='errMsg'>Please Choose course</span>");
  			return false;
  		}
  		else if($('input[name="college"]').val()==""){
  			if($('input[name="college"]').next(".errMsg").text()=="")
  			$('input[name="college"]').after("<span class='errMsg'>Please Enter Your college</span>");
  			return false;
  		}
  		else{
  			return true;
  		}
  	});

$(window).on('scroll',function(){
   //var box=$(".help-box").offset();
   
 if($(window).scrollTop()>200 && $(window).width()>767){
      $('.navigation-bar').addClass('sticky')
 }
 else
 {
 $('.navigation-bar').removeClass('sticky')
 }
})
$(".collapseBox table").on("click",function(){
	 
            $(this).parent(".collapseBox").children(".collapseContent").slideToggle();
            $(this).parent(".collapseBox").toggleClass("active");

            /*if($(this).find("i").class()=="fa fa-sort-down"){
                $(this).find("i").removeClass("fa fa-sort-down");
                $(this).find("i").addClass("fa fa-sort-up");
            }
            else{
                $(this).find("i").removeClass("fa fa-sort-up");
                $(this).find("i").addClass("fa fa-sort-down");
            }*/
        });
        });
    
</script>
