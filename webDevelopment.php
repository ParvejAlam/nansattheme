<?php require_once("header.php"); ?>
<!-- course Header start tag-->
<div class="courseHeader">
    <div class="container">
            <h1>Web Development</h1>
            <div class="bannerTagLine">PHP & mySQL</div>
            <div class="taglineBorder">
                <div>
                    <div></div>
                </div>
            </div>
    </div>
</div>
<!-- course Header end tag -->
<!-- breadcrumb start tag-->
<nav class="customBreadcrumb">      
    <div class="container">
        <a href="javascript:void(0);">Home</a><i class="fa fa-chevron-right"></i><a href="javascript:void(0);">All courses</a><i class="fa fa-chevron-right"></i> Web Development
    </div>

</nav>
<!-- breadcrumb end tag -->

<!-- Main Content Start from here -->
<div class="container">
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="boxContainer">
                <h2 class="courseTitle">Web Development</h2>
                <div class="singleCourse">
                    <div class="clearfix">
                        <div class="pull-left meta_pull">

                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa-icon-stm_icon_teacher"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Faculty</div>
                                            <div class="value">Mohit Bansal</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                     
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_users"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Students</div>
                                            <div class="value">30 per batch</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_clock"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Duration</div>
                                            <div class="value">3 Months</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_bullhorn"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Classes</div>
                                            <div class="value">25</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_earth"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Live Project</div>
                                            <div class="value">1</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-file-text-o"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Cetification</div>
                                            <div class="value"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                                 
                                            
                        </div> <!-- meta pull -->
                        

                    </div>
                    <div class="clear"></div>
                </div>
                <div class="courseImg">
                    <img src="assets/img/webDevelopmentBanner.png" alt="Course Name">
                </div>
            </div>

        </div>  <!-- col-xs-9 end -->
        <div class="col-lg-3 col-md-3">     
            <div class="topRight">
                <div class="row">
                    <div class="col-xs-12">
                        <label>PRICE</label>
                        <p><i class="fa fa-inr"></i> 15,000/-</p>
                    </div>
                </div>
                <a href="javascript:;" class="affiliate">Enroll Now</a>
                <div class="enrollBox">
                    <form method="post" action="enrollExec.php">
                        <input type="text" name="name" placeholder="Whats your name..."> 
                        <input type="text" name="contact" placeholder="Whats your Phone..."> 
                        <input type="text" name="emailId" placeholder="Whats your Email..."> 
                        <input type="hidden" name="course" value="web Development"> 
                        <input type="text" name="college" placeholder="College/Company Name..."> 
                        <input type="submit" name="btn" value="Submit">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <section class="content">
                    <h3 class="sectionTitle">Target Audience</h3>
                    <div class="">
                        <p>
                            This course is specially designed for the B.Tech/B.E, M.Tech/M.E and
all other IT related Graduates and Post Graduate students who are interested in
learning web development using PHP framework.
                        </p>
                    </div>
                </section>
				<section class="content">
                    <h3 class="sectionTitle">Live Project Work</h3>
                    <div class="">
                        <p>
                           Live project is the phase when you finally implement most of the things
that you have learnt during your software training. Software development is
more than just coding. Before you write even a single line of code, it requires
careful analysis of the requirements, gathering information, preparing the
necessary documentation which requires understanding the live project using
Software Development Life Cycle. So you have to learn tricks to produce bulk
output on time maintaining the right design quality or coding standard. That is
the significance of Live Project Training. We assure that our Live Project
Training will impart the confidence in students to work on real time projects.
                        </p>
                    </div>
                </section>
				<section class="content ">
                    <h3 class="sectionTitle">Mission</h3>
                    <div class="">
                        <p>
                            Professionalism has conquered the job scenario and companies seek for
well qualified, professional and skilled manpower. Keeping in view this demand
of companies we groom students in such a way that they will be second to none.
Quality Education and Performance Oriented Training is our motto.
                        </p>
                    </div>
                </section>

                <div class="multiseparator m-t-60"></div> 
                <section class="m-t-20">
                    <h3 class="sectionTitle">CURRICULUM</h3>
                    <!-- <h5 class="boxHeading">Section 1: Introduction to Handstands</h5> -->
                    <div class="collapseGroup">
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>1</td>
                                    <td><strong>HTML 5- The Static Web Page Creation </strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>
                           
                        </div>
						<div class="collapseBox">
                            <table>
                                <tr>
                                    <td>2</td>
                                    <td><strong>CSS 3 - The Presentation Semantics </strong></td>
                                    <td>12 Classes</td>
                                </tr>
                            </table>
                         
                        </div>
						<div class="collapseBox">
                            <table>
                                <tr>
                                    <td>3</td>
                                    <td><strong>JavaScript - The Interpreted Programming Language </strong></td>
                                    <td>5 Classes</td>
                                </tr>
                            </table>
                          
                        </div>
						<div class="collapseBox">
                            <table>
                                <tr>
                                    <td>4</td>
                                    <td><strong>jQuery - Write Less Do More </strong></td>
                                    <td>4 Classes</td>
                                </tr>
                            </table>
                       
                        </div>
						<div class="collapseBox">
                            <table>
                                <tr>
                                    <td>5</td>
                                    <td><strong>Bootstrap</strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>

                        </div>
						<div class="collapseBox">
                            <table>
                                <tr>
                                    <td>6</td>
                                    <td><strong>PHP – Understanding the Preliminaries</strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>

                        </div>
						<div class="collapseBox">
                            <table>
                                <tr>
                                    <td>7</td>
                                    <td><strong>PHP - The Core Logics and Techniques </strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>

                        </div>
						<div class="collapseBox">
                            <table>
                                <tr>
                                    <td>8</td>
                                    <td><strong>PHP – File Handling. The Plain Repository of Data </strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>

                        </div>
						<div class="collapseBox">
                            <table>
                                <tr>
                                    <td>9</td>
                                    <td><strong>PHP and MySQL - The Structured Repository</strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>

                        </div>
						<div class="collapseBox">
                            <table>
                                <tr>
                                    <td>10</td>
                                    <td><strong>Learn More Advanced Techniques in PHP</strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>

                        </div>
						<div class="collapseBox">
                            <table>
                                <tr>
                                    <td>11</td>
                                    <td><strong>Working with XML and JSON</strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>

                        </div>
						<div class="collapseBox">
                            <table>
                                <tr>
                                    <td>12</td>
                                    <td><strong>Frameworks(CodeIgniter,Laravel)</strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>

                        </div>
						<div class="collapseBox">
                            <table>
                                <tr>
                                    <td>13</td>
                                    <td><strong>Live Project</strong></td>
                                    <td>4 Classes</td>
                                </tr>
                            </table>

                        </div>
                    </div>


                </section>
                <div class="multiseparator m-t-60"></div>
                <section class="m-t-30">
                    <h3 class="sectionTitle">ABOUT INSTRUCTOR</h3>
                    <a href="javascript:void(0);">
                        <div class="instructorBox">
                            <img src="assets/img/instructor.jpg" alt="instructor">
                            <div>
                                <div class="name">
Mohit Bansal</div>
                                <p class="desc">Chief Technology Officer</p>
                            </div>
                            <div class="socialLink">
                                <a href="https://www.facebook.com/mohit.bansal.520">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/in/mohit-bansal-bb463670/">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            <!-- 
                                <a href="javascript:void(0);">
                                    <i class="fa fa-twitter"></i>
                                </a>
                                <a href="javascript:void(0);">
                                    <i class="fa fa-google-plus"></i>
                                </a> 
                            -->
                            </div>
                        </div>
                    </a>
                <!-- 
                    <div class="clear"></div>
                    <p class="instructorDescription">
                        Before engineering, I developed and implemented CI (Continuous Improvement) management systems in companies across America in various industries to include a military finance organization.
                    </p>
                 -->
             </section>
                <div class="multiseparator m-t-50"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

    </div>
</div>
</div>
<?php require_once("footer.php"); ?>