<?php require_once("header.php"); ?>
<!-- course Header start tag-->
<div class="courseHeader">
    <div class="container">
            <h1>Mean Stack Development</h1>
            <div class="bannerTagLine">Node.js, React.js & Cloud Services</div>
            <div class="taglineBorder">
                <div>
                    <div></div>
                </div>
            </div>
    </div>
</div>
<!-- course Header end tag -->
<!-- breadcrumb start tag-->
<nav class="customBreadcrumb">      
    <div class="container">
        <a href="javascript:void(0);">Home</a><i class="fa fa-chevron-right"></i><a href="javascript:void(0);">All courses</a><i class="fa fa-chevron-right"></i> Mean Stack Development
    </div>

</nav>
<!-- breadcrumb end tag -->

<!-- Main Content Start from here -->
<div class="container">
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="boxContainer">
                <h2 class="courseTitle">Mean Stack Development</h2>
                <div class="singleCourse">
                    <div class="clearfix">
                        <div class="pull-left meta_pull">

                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa-icon-stm_icon_teacher"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Faculty</div>
                                            <div class="value">Mohit Bansal</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_category"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Category</div>
                                            <div class="value">Machine Learning</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_users"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Students</div>
                                            <div class="value">30 per batch</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_clock"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Duration</div>
                                            <div class="value">3 Months</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_bullhorn"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Classes</div>
                                            <div class="value">24</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_earth"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Live Project</div>
                                            <div class="value">3</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-file-text-o"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Cetification</div>
                                            <div class="value"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                                 
                                            
                        </div> <!-- meta pull -->
                        

                    </div>
                    <div class="clear"></div>
                </div>
                <div class="courseImg">
                    <img src="assets/img/meanBanner.png" alt="Course Name">
                </div>
            </div>

        </div>  <!-- col-xs-9 end -->
        <div class="col-lg-3 col-md-3">     
            <div class="topRight">
                <div class="row">
                    <div class="col-xs-12">
                        <label>PRICE</label>
                        <p><i class="fa fa-inr"></i> 22,000/-</p>
                    </div>
                </div>
                <a href="javascript:;" class="affiliate">Enroll Now</a>
                <div class="enrollBox">
                    <form method="post" action="enrollExec.php">
                        <input type="text" name="name" placeholder="Whats your name..."> 
                        <input type="text" name="contact" placeholder="Whats your Phone..."> 
                        <input type="text" name="emailId" placeholder="Whats your Email..."> 
                        <input type="hidden" name="course" value="mean"> 
                        <input type="text" name="college" placeholder="College/Company Name..."> 
                        <input type="submit" name="btn" value="Submit">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <!-- <section class="content m-t-40">
                    <h3 class="sectionTitle">COURSE DESCRIPTION</h3>
                    <div class="m-b-30">
                        <p>
                            This MEAN Stack Developer Master’s Program is designed to give you a thorough understanding of MEAN stack technologies, including MongoDB, Express.js, Angular, Node.js, JavaScript, HTML, and CSS. The MEAN Stack learning path ensures that you gain expertise in the various components of MEAN stack, from designing and building RESTful APIs using Node.js, Express, and MongoDB to developing modular, maintainable Single Page Applications using AngularJS. The overall objective of the full-stack developer course is to enable you to build superior applications using MEAN stack. As electives, the learning path comprises JavaScript, HTML, and CSS, which are all additional skillsets helpful in becoming a MEAN stack champion.

                        </p>
                    </div>
                </section>

                <div class="multiseparator m-t-60"></div> -->
                <section class="m-t-20">
                    <h3 class="sectionTitle">CURRICULUM</h3>
                    <!-- <h5 class="boxHeading">Section 1: Introduction to Handstands</h5> -->
                    <div class="collapseGroup">
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>1</td>
                                    <td><strong>Introduction to Mean Stack Technologies <i class="fa fa-sort-down"></i></strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>A talk about full stack development in general</li>
                                    <li>An introduction to the MEAN technologies</li>
                                    <li>An outline of our approach to this section</li>
                                    <li>A demonstration of the application you'll build through this section of the course</li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>2</td>
                                    <td><strong>NodeJs <i class="fa fa-sort-down"></i></strong></td>
                                    <td>5 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>Introduction of NodeJs</li>
                                    <li>Core Concepts</li>
                                    <li>Installation & Application</li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>3</td>
                                    <td><strong>ReactJs <i class="fa fa-sort-down"></i></strong></td>
                                    <td>5 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>Introduction of NodeJs</li>
                                    <li>Core Concepts</li>
                                    <li>Installation & Application</li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>4</td>
                                    <td><strong>DataBases <i class="fa fa-sort-down"></i></strong></td>
                                    <td>3 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>Cassandra</li>
                                    <li>MongoDB</li>
                                    <li>RDBMS</li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>5</td>
                                    <td><strong>Cloud Services <i class="fa fa-sort-down"></i></strong></td>
                                    <td>5 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>Application</li>
                                    <li>Deployment</li>
                                    <li>Benefits(Aws, Azure, Google Cloud)</li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>6</td>
                                    <td><strong>Final Project <i class="fa fa-sort-down"></i></strong></td>
                                    <td>4 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>DataBase Design</li>
                                    <li>Push application on ReactJs</li>
                                    <li>Stock application on NodeJs</li>
                                </ul>
                            </div>
                        </div>
                    </div>


                </section>
                <div class="multiseparator m-t-60"></div>
                <section class="m-t-30">
                    <h3 class="sectionTitle">ABOUT INSTRUCTOR</h3>
                    <a href="javascript:void(0);">
                        <div class="instructorBox">
                            <img src="assets/img/instructor.jpg" alt="instructor">
                            <div>
                                <div class="name">
Mohit Bansal</div>
                                <p class="desc">Chief Technology Officer</p>
                            </div>
                            <div class="socialLink">
                                <a href="https://www.facebook.com/mohit.bansal.520">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/in/mohit-bansal-bb463670/">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            <!-- 
                                <a href="javascript:void(0);">
                                    <i class="fa fa-twitter"></i>
                                </a>
                                <a href="javascript:void(0);">
                                    <i class="fa fa-google-plus"></i>
                                </a> 
                            -->
                            </div>
                        </div>
                    </a>
                <!-- 
                    <div class="clear"></div>
                    <p class="instructorDescription">
                        Before engineering, I developed and implemented CI (Continuous Improvement) management systems in companies across America in various industries to include a military finance organization.
                    </p>
                 -->
             </section>
                <div class="multiseparator m-t-50"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">


    </div>
</div>
</div>
<?php require_once("footer.php"); ?>