<?php require_once("header.php"); ?>
<!-- course Header start tag-->
<div class="courseHeader">
    <div class="container">
            <h1>Algorithms & AI</h1>
            <div class="bannerTagLine">Machine Learning with Python</div>
            <div class="taglineBorder">
                <div>
                    <div></div>
                </div>
            </div>
    </div>
</div>
<!-- course Header end tag -->
<!-- breadcrumb start tag-->
<nav class="customBreadcrumb">      
    <div class="container">
        <a href="javascript:void(0);">Home</a><i class="fa fa-chevron-right"></i><a href="javascript:void(0);">All courses</a><i class="fa fa-chevron-right"></i> Algorithms & AI
    </div>

</nav>
<!-- breadcrumb end tag -->

<!-- Main Content Start from here -->
<div class="container">
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="boxContainer">
                <h2 class="courseTitle">Algorithms & AI</h2>
                <div class="singleCourse">
                    <div class="clearfix">
                        <div class="pull-left meta_pull">

                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa-icon-stm_icon_teacher"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Faculty</div>
                                            <div class="value">Mohit Bansal</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_category"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Category</div>
                                            <div class="value">Machine Learning</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_users"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Students</div>
                                            <div class="value">30 per batch</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_clock"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Duration</div>
                                            <div class="value">3 Months</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_bullhorn"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Classes</div>
                                            <div class="value">30</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-icon-stm_icon_earth"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Live Project</div>
                                            <div class="value">4</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="pull-left">
                                <a href="javascript:;">
                                    <div class="meta-unit teacher clearfix">
                                        <div class="pull-left">
                                            <i class="fa fa-file-text-o"></i>
                                        </div>
                                        <div class="meta_values">
                                            <div class="label">Cetification</div>
                                            <div class="value"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                                 
                                            
                        </div> <!-- meta pull -->
                        

                    </div>
                    




                    <div class="clear"></div>
                </div>
                <div class="courseImg">
                    <img src="assets/img/artificialIntellinceBanner.png" alt="Course Name">
                </div>
            </div>

        </div>  <!-- col-xs-9 end -->
        <div class="col-lg-3 col-md-3">     
            <div class="topRight">
                <div class="row">
                    <div class="col-xs-12">
                        <label>PRICE</label>
                        <p><i class="fa fa-inr"></i> 20,000/-</p>
                    </div>
                </div>
                <a href="javascript:;" class="affiliate">Enroll Now</a>
                <div class="enrollBox">
                    <form method="post" action="enrollExec.php">
                        <input type="text" name="name" placeholder="Whats your name..."> 
                        <input type="text" name="contact" placeholder="Whats your Phone..."> 
                        <input type="text" name="emailId" placeholder="Whats your Email...">
                        <input type="hidden" name="course" value="ai"> 
                        <input type="text" name="college" placeholder="College/Company Name..."> 
                        <input type="submit" name="btn" value="Submit">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <!-- 
                <section class="content m-t-40">
                    <h3 class="sectionTitle">COURSE DESCRIPTION</h3>
                    <div class="m-b-30">
                        <p>
                            Nansat Commerce Pvt. Ltd. offers Artificial Intelligence as a 360 degree service. We help companies with developing a range of AI solutions that learn and think like humans using Natural Language Processing (NLP), Speech Recognition and Machine Learning feature. Nansat Commerce Pvt. Ltd. has core ability to integrate artificial intelligence using cognitive technology and semantic technology with any of your current and future business modules, solutions or third party applications across a full spectrum of industries, from Big Data, Business Intelligence, Analytics, IT Technology and others. We enable organization to connect artificial intelligence with your day to day working environment. The AI solutions offered by Nansat Commerce Pvt. Ltd. empowers you with business growth by minimizing your labor and infrastructure cost.
                        </p>
                        <p>
                            Nansat Commerce Pvt. Ltd. have years of experience in analyzing customers requirement and delivering industry best applications to meet their business needs. We have strong knowledge in designing, implementing and integrating Artificial Intelligence technology within the customer’s business environment whether it be Healthcare, Telecommunications, Manufacturing, Customer Services, Education, Financial Services, Mobile, or Intelligent Kiosks etc.
                        </p>
                    </div>
                </section> 

                <div class="multiseparator m-t-60"></div>
            -->
                <section class="m-t-20">
                    <h3 class="sectionTitle">CURRICULUM</h3>
                    <!-- <h5 class="boxHeading">Section 1: Introduction to Handstands</h5> -->
                    <div class="collapseGroup">
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>1</td>
                                    <td><strong>Introduction To Python  <i class="fa fa-sort-down"></i></strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>Introductory Remarks about Python</li>
                                    <li>A Brief History of Python</li>
                                    <li>How python is differ from other languages</li>
                                    <li>Python Versions</li>
                                    <li>Installing Python</li>
                                    <li>IDLE</li>
                                    <li>Getting Help</li>
                                    <li>How to execute Python program</li>
                                    <li>Writing your first program </li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>2</td>
                                    <td><strong>Algorithm  <i class="fa fa-sort-down"></i></strong></td>
                                    <td>7 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <!-- <p>
                                    an algorithm is a generic, step-by-step list of instructions for solving a problem. It is a method for solving any instance of the problem such that given a particular input, the algorithm produces the desired result. A program, on the other hand, is an algorithm that has been encoded into some programming language. There may be many programs for the same algorithm, depending on the programmer and the programming language being used.
                                </p> 
                                <p>
                                    The following topic will be discussed in these classes :
                                </p> -->
                                <ul>
                                    <li>Array</li>
                                    <li>Queue</li>
                                    <li>Stack</li>
                                    <li>Linked List</li>
                                    <li>Tree</li>
                                    <li>Graph</li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>3</td>
                                    <td><strong>Introduction to Machine Learning & Tools  <i class="fa fa-sort-down"></i></strong></td>
                                    <td>2 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <!-- <p>
                                    Machine learning, a type of artificial intelligence that "learns" as it identifies new patterns in data, enables data scientists to effectively pinpoint revenue opportunities and create strategies to improve customer experiences using information hidden in huge data sets.
                                </p> -->
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>4</td>
                                    <td><strong>Supervised Learning <i class="fa fa-sort-down"></i></strong></td>
                                    <td>5 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>K Nearest Neighbours</li>
                                    <li>SVM</li>
                                    <li>Data Handling</li>
                                    <li>Feature Extraction and Selection</li>
                                    <li>Principal Component Analysis</li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>5</td>
                                    <td><strong>UnSupervised Learning <i class="fa fa-sort-down"></i></strong></td>
                                    <td>5 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>Clustering - Flat and Hierarchical</li>
                                    <li>KMeans</li>
                                    <li>Mean Shift</li>
                                    <li>Projects on Unsupervised Learning Algorithms</li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>5</td>
                                    <td><strong>Deep Learning & Neural Network <i class="fa fa-sort-down"></i></strong></td>
                                    <td>5 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>Basic Neural Networks</li>
                                    <li>Deep Networks and their structure</li>
                                    <li>CNN & RNN</li>
                                    <li>Basic of Deep Reinforcement Learning</li>
                                    <li>Natural Language Processing</li>
                                </ul>
                            </div>
                        </div>
                        <div class="collapseBox">
                            <table>
                                <tr>
                                    <td>6</td>
                                    <td><strong>Final Project <i class="fa fa-sort-down"></i></strong></td>
                                    <td>4 Classes</td>
                                </tr>
                            </table>
                            <div class="collapseContent">
                                <ul>
                                    <li>Context Analysis</li>
                                    <li>Recommendation Engine</li>
                                    <li>Image Processing</li>
                                    <li>Related Data</li>
                                </ul>
                            </div>
                        </div>
                    </div>


                </section>
                <div class="multiseparator m-t-60"></div>
                <section class="m-t-30">
                    <h3 class="sectionTitle">ABOUT INSTRUCTOR</h3>
                    <a href="javascript:void(0);">
                        <div class="instructorBox">
                            <img src="assets/img/instructor.jpg" alt="instructor">
                            <div>
                                <div class="name">
Mohit Bansal</div>
                                <p class="desc">Chief Technology Officer</p>
                            </div>
                            <div class="socialLink">
                                <a href="https://www.facebook.com/mohit.bansal.520">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/in/mohit-bansal-bb463670/">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            <!-- 
                                <a href="javascript:void(0);">
                                    <i class="fa fa-twitter"></i>
                                </a>
                                <a href="javascript:void(0);">
                                    <i class="fa fa-google-plus"></i>
                                </a> 
                            -->
                            </div>
                        </div>
                    </a>
                <!-- 
                    <div class="clear"></div>
                    <p class="instructorDescription">
                        Before engineering, I developed and implemented CI (Continuous Improvement) management systems in companies across America in various industries to include a military finance organization.
                    </p>
                 -->
             </section>
                <div class="multiseparator m-t-50"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">


    </div>
</div>
</div>
<?php require_once("footer.php"); ?>