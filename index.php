<?php require_once("header.php"); ?>
<!-- slider -->
<div class="container-fluid slider" id="home">
    <div class="row">
	   
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators 
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>-->

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item ">
        
        <div class="carousel-caption container">
          <h3>FALL IN LOVE WITH CODING</h3>
          <p>Explore the world of competitive programming</p>
        </div>
      </div>

      <div class="item active">
        
        <div class="carousel-caption container">
          <h3>WORK ON LIVE PROJECTS & GET CERTIFICATION
</h3>
          <p>Batches commencing 1st May 2018.<br> Limited Seats Available
</p>
        </div>
      </div>
    
 
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      
      <span class="sr-only">Next</span>
    </a>
  </div>
	</div>
</div>
<!--slider -->
<!-- success stories -->
	<div class="container-fluid success-stories" id="success-stories">
		<div class="row">
			<div class="container">
			   <div class="row">
				   <div class="col-xs-12 heading">
					  <h1>SUCCESS STORIES</h1>
					</div>
			   </div>
			   <div class="row">
				  <div class="col-xs-12 box-main">
					 <div class="row">
						<div class="col-sm-2 col-xs-6 box">
							<img src="assets/img/student-pic1.jpg">
							<h5>Mayank Shorie</h5>
							<p>YMCAUST</p>
							<p class="text">Selected In <img src="assets/img/samsung.png"></img></p>
						</div>
						<div class="col-sm-2 col-xs-6 box">
							<img src="assets/img/student-pic2.jpg">
							<h5>Samiksha Bansal</h5>
							<p>YMCAUST</p>
							<p class="text">Selected In <img src="assets/img/amazon.png"></img></p>
						</div>
						<div class="col-sm-2 col-xs-6 box">
							<img src="assets/img/student-pic3.jpg">
							<h5>Jatin Bansal</h5>
							<p>YMCAUST</p>
							<p class="text">Selected In <img src="assets/img/amazon.png"></img></p>
						</div>
						<div class="col-sm-2 col-xs-6 box">
							<img src="assets/img/student-pic4.jpg">
							<h5>Bhanu Aggarwal</h5>
							<p>NIT Kurukshetra</p>
							<p class="text">Selected In <img src="assets/img/uber.png"></img></p>
						</div>
						<div class="col-sm-2 col-xs-6 box">
							<img src="assets/img/student-pic5.jpg">
							<h5>Himanshu Goyal</h5>
							<p>Amity University</p>
							<p class="text">Selected In <img src="assets/img/ephespft.png"></img></p>
						</div>
						<div class="col-sm-2 col-xs-6 box">
							<img src="assets/img/student-pic6.jpg">
							<h5>Mayank Vashisht</h5>
							<p>BSAITM, Faridabad
	</p>
	<p class="text">Selected In <img src="assets/img/sapient.png"></img></p>
						</div>
					 </div>
				  </div>
				</div>
			</div>
		</div>
	</div>
<!-- success stories -->

<!-- courses batch-->
<div class="container-fluid courses" id="courses">
    <div class="row">
	    <div class="container">
		   <div class="row">
		       <div class="col-xs-12 heading">
			      <h1>UPCOMING BATCHES</h1>
			   </div>
		   </div>
		   <div class="row">
		       <div class="col-xs-12 courses-details">
			     <div class="row">
			      <div class="col-md-3 col-sm-6 box">
				     <div class="row">
					     <div class="col-xs-12 img-box">
						   <a href="artificialIntelligence.php">
						     <img src="assets/img/Algorithms&AI.jpg">
						   </a>
						   <div class="price">
						     <span><i class="fa fa-inr"></i> INR 20000</span>
						   </div>
						 </div>
					 </div>
					 <div class="row">
					 <div class="col-xs-12 contents">
					   <a href="artificialIntelligence.php" class="title">Algorithms & A.I</a>
					   <p>Machine Learning with Python</p>
					 </div>
					</div>
					<div class="row">
					   <div class="col-xs-12 footer">
					   <div class="pull-left">
						<i class="fa-icon-stm_icon_user"></i> <span>30 Classes</span>
						</div>
						 <div class="pull-right">
						   <span class="rating"></span>
						</div>
						<div class="more-details">
						   <a href="artificialIntelligence.php">Show More</a>
						</div>
					   </div>
					</div>
				  </div>
				   <div class="col-md-3 col-sm-6 box">
				     <div class="row">
					     <div class="col-xs-12 img-box">
						   <a href="meanStackDevelopment.php">
						     <img src="assets/img/meanstack.png">
						   </a>
						   <div class="price">
						     <span><i class="fa fa-inr"></i> INR 22000</span>
						   </div>
						 </div>
					 </div>
					 <div class="row">
					 <div class="col-xs-12 contents">
					   <a href="meanStackDevelopment.php" class="title">Mean Stack Development</a>
					   <p>Node.js, React.js & Cloud Services</p>
					 </div>
					</div>
					<div class="row">
					   <div class="col-xs-12 footer">
					   <div class="pull-left">
						<i class="fa-icon-stm_icon_user"></i> <span>24 Classes</span>
						</div>
						 <div class="pull-right">
						   <span class="rating"></span>
						</div>
						<div class="more-details">
						   <a href="meanStackDevelopment.php">Show More</a>
						</div>
					   </div>
					</div>
				  </div>
				   <div class="col-md-3 col-sm-6 box">
				     <div class="row">
					     <div class="col-xs-12 img-box">
						   <a href="blockChain.php">
						     <img src="assets/img/blockchain.jpg">
						   </a>
						   <div class="price">
						     <span><i class="fa fa-inr"></i> INR 25000</span>
						   </div>
						 </div>
					 </div>
					 <div class="row">
					 <div class="col-xs-12 contents">
					   <a href="blockChain.php" class="title">BlockChain Development
</a>
					   <p>Machine Learning with Python</p>
					 </div>
					</div>
					<div class="row">
					   <div class="col-xs-12 footer">
					   <div class="pull-left">
						<i class="fa-icon-stm_icon_user"></i> <span>30 Classes</span>
						</div>
						 <div class="pull-right">
						   <span class="rating"></span>
						</div>
						<div class="more-details">
						   <a href="blockChain.php">Show More</a>
						</div>
					   </div>
					</div>
				  </div>
				   <div class="col-md-3 col-sm-6 box">
				     <div class="row">
					     <div class="col-xs-12 img-box">
						   <a href="webDevelopment.php">
						     <img src="assets/img/web-development.png">
						   </a>
						   <div class="price">
						     <span><i class="fa fa-inr"></i> INR 15000</span>
						   </div>
						 </div>
					 </div>
					 <div class="row">
					 <div class="col-xs-12 contents">
					   <a href="webDevelopment.php" class="title">Web Design & Development
</a>
					   <p>PHP & mySQL
</p>
					 </div>
					</div>
					<div class="row">
					   <div class="col-xs-12 footer">
					   <div class="pull-left">
						<i class="fa-icon-stm_icon_user"></i> <span>28 Classes</span>
						</div>
						 <div class="pull-right">
						   <span class="rating"></span>
						</div>
						<div class="more-details">
						   <a href="webDevelopment.php">Show More</a>
						</div>
					   </div>
					</div>
				  </div>
				  
			   </div>
		   </div>
		</div>
	</div>
</div>
</div>
<!-- courses batch-->

<!-- achivements -->
<div class="container-fluid achievement" id="achievement">
    <div class="row">
	    <div class="container">
		   <div class="row">
		       <div class="col-xs-12 heading">
			      <h1>OUR ACHIEVEMENTS</h1>
				  <p>Some statistics about our Center</p>
				  
			   </div>
		   </div>
		   <div class="row">
		      <div class="col-xs-12 achievement-details">
			       <div class="row">
				       <div class="col-sm-3 col-xs-12 box">
				          <div class="row">
						      <div class="col-xs-12 icon">
							  <i style="font-size: 65px;" class="fa fa-icon-stm_icon_earth"></i>
							  </div>
						  </div>
						  <div class="row">
						      <div class="col-xs-12 contents">
							     <h1><span class="count" data-count="50">50</span>+ YEARS</h1>
								 <h5>COLLECTIVE EXPERIENCE OF FACULTY</h5>
							  </div>
						  </div>
				       </div>
				       <div class="col-sm-3 col-xs-12 box">
				          <div class="row">
						      <div class="col-xs-12 icon">
							  <i style="font-size: 71px;" class="fa fa-icon-stm_icon_alarm"></i>
							  </div>
						  </div>
						  <div class="row">
						      <div class="col-xs-12 contents">
							     <h1><span class="count" data-count="27">27</span> LACS</h1>
								 <h5>HIGHEST SALARY PLACED STUDENT</h5>
							  </div>
						  </div>
				       </div>
				       <div class="col-sm-3 col-xs-12 box">
				          <div class="row">
						      <div class="col-xs-12 icon">
							  <i style="font-size: 65px;" class="fa fa-icon-stm_icon_briefcase"></i>
							  
							  </div>
						  </div>
						  <div class="row">
						      <div class="col-xs-12 contents">
							     <h1><span class="count" data-count="10">10</span>+ COMPANIES</h1>
								 <h5>DIRECTLY HIRING OUR STUDENTS</h5>
							  </div>
						  </div>
				       </div>
				       <div class="col-sm-3 col-xs-12 box">
				          <div class="row">
						      <div class="col-xs-12 icon">
							    <i style="font-size: 65px;" class="fa fa-icon-stm_icon_users"></i>
							  </div>
						  </div>
						  <div class="row">
						      <div class="col-xs-12 contents">
							     <h1><span class="count" data-count="95">95</span>%</h1>
								 <h5>PERCENTAGE STUDENTS PLACED</h5>
							  </div>
						  </div>
				       </div>
				   </div>
			  </div>
		   </div>
		</div> 
    </div>
</div>	
<!-- achivements -->
<!-- slider help box -->
<div class="container-fluid  help-box">
  <div class="row">
    <div class="container">
      <div class="row">
      <div class="col-xs-12 box-main">
      <div class="row">
	    <div class="col-sm-3 col-xs-12 box">
		   <i  class="fa fa-icon-stm_icon_graduation-hat"></i>
		   <h3>HIGHEST PAID TECH SKILLS</h3>
		   <p>Get trained on skills that are highest in demand in the Industry e.g. AI, Block Chain, Machine Learning, Meanstack, Cloud Computing & Deep Tech</p>
		</div>
	    <div class="col-sm-3 col-xs-12 box">
		<i  class="fa fa-icon-stm_icon_book"></i>
		<h3>GET HIRED BY DREAM COMPANIES</h3>
		   <p>Our trained students have got offers from some of the world's top companies like Amazon, Sapient, Accenture, Salesforce etc</p>
		</div>
	    <div class="col-sm-3 col-xs-12 box">
		<i  class="fa fa-icon-stm_icon_diamond"></i>
		<h3>TRAINING BY INDUSTRY EXPERTS</h3>
		   <p>Our faculty is from Industry and we thrive by providing hands on practical training on every concept taught in the classroom</p>
		</div>
	    <div class="col-sm-3 col-xs-12 box">
		<i  class="fa fa-icon-stm_icon_license"></i>
		<h3>CERTIFICATION</h3>
		   <p>Upon successful completion receive an internship certificate showing your achievement for completing one of our rigorous program.</p>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
<!-- slider help box -->

<!-- enroll -->
  <div class="container-fluid enroll" id="enroll">
    <div class="row">
	    <div class="container">
		   <div class="row">
		       <div class="col-xs-12 enroll-details">
			      <div class="row">
				     <div class="col-xs-12 col-sm-6 col-md-7 box-left">
					   <h1>ENROLL YOURSELF NOW <br> FOR THE <span>UPCOMING BATCHES</span></h1>
					    <h3>Courses Commencing 1st May 2018</h3>
						<h2>Limited Seats Per Batch</h2>
					 </div>
					 <div class="col-xs-12 col-sm-6 col-md-5 box-right">
					   <div class="row">
					    <div class="col-xs-12 enroll-form">
						   <h2>Enroll Now</h2>
						   <form method="post" action="enrollExec.php">
							   <input type="text" name="name" class="form-control" placeholder="Whats your name..."> 
							   <input type="text" name="contact" class="form-control" placeholder="Whats your Phone..."> 
							   <input type="text" name="emailId" class="form-control" placeholder="Whats your Email..."> 
							   <select name="course" class="form-control">
							   		<option value="" class="hide">Select Course Name</option>
							   		<option value="ai">ALGORITHMS & AI</option>
							   		<option value="mean">MEAN STACK DEVELOPMENT</option>
							   		<option value="block Chain">BLOCK CHAIN</option>
							   		<option value="web Development">WEB DEVELOPMENT</option>
							   </select>
							   <input type="text" name="college" class="form-control" placeholder="Whats your College/Company Name..."> 
							   <button class="btn btn-custom" type="submit" name="btn">Get It</button>
						   </form>
						</div>
						</div>
					 </div>
					 
				  </div>
			   </div>
			</div>
		</div>
	</div>
  </div>
 
<!-- enroll -->
<?php require_once("footer.php"); ?>

<!-- footer -->
<script>
$(document).ready(function(){
  // Add scrollspy to <body>
  $('body').scrollspy({target: ".navbar", offset: 50});   

  // Add smooth scrolling on all links inside the navbar
  $("#myNavbar a").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top - 75
      }, 800, );
    } 
	// End if
	$("#myNavbar a").removeClass('active');
	$(this).addClass('active');
  });

var a = 0;
$(window).scroll(function() {

  var oTop = $('.count').offset().top - window.innerHeight;
  if (a == 0 && $(window).scrollTop() > oTop) {
    $('.count').each(function() {
      var $this = $(this),
        countTo = $this.attr('data-count');
      $({
        countNum: $this.text(a)
      }).animate({
          countNum: countTo
        },

        {

          duration: 2000,
          easing: 'swing',
          step: function() {
            $this.text(Math.floor(this.countNum));
          },
          complete: function() {
            $this.text(this.countNum);
            //alert('finished');
          }

        });
    });
    a = 1;
  }

});
})

</script>
</body>
</html>